function soloNumeros(e)
{
  var key = window.Event ? e.which : e.keyCode
  return ((key >= 48 && key <= 57) || (key==8))
}
function validacionPostal()
{
    mensaje = document.getElementById('mensaje').value;
    foto = document.getElementById('correcto').value;
    if(mensaje == "")
    {
      toastError("Agrega mensaje a tu postal");
    }
    else if(foto == "0")
    {
      toastError("Ya te registraste, ahora comparte tu postal.")
    }
    else if (foto!="3") {
      toastError("Seleciona una foto para tu postal");
    }
    else
    {

        return true;
    }
}
function validacionRegistro()
{
  nombre  = document.getElementById('name').value;
  apellido  = document.getElementById('lastName').value;
  cedula  = document.getElementById('ci').value;
  email  = document.getElementById('email').value;

  if(validacionPostal())
  {

    if(nombre == "" || apellido == "" || cedula == "" || email == "" )
    {
      toastError("Primero llene los campos con su información");
    }
    else
    {
        actualizar();
        document.getElementById('correcto').value="0";
        document.getElementById('contfb').className += ' active';
        document.getElementById('contEv').className += ' active';
        document.getElementById('contDes').className += ' active';
    }
  }

}
function validacionEmail()
{
  email = document.getElementById("destinatario").value;
  var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}
function validarEnviar()
{

  if(document.getElementById("cuerpo").value=="" || document.getElementById("destinatario").value=="")
  {
    toastError("Primero llene los campos solicitados");
  }
  else if(validacionEmail())
  {
    enviarCorreo();
  }
  else if(!validacionEmail())
  {
    toastError("Dirección de correo no válida","Error");
  }

}
