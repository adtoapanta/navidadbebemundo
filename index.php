<!DOCTYPE html>
<html lang="es" style="background-color:#FFF">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<meta name="description" content="Navidad Bebemundo">
	<meta name="author"      content="Ing. Luis Rocha www.crsoft.org">
	<meta property="og:url"           content= "http://navidad.bebemundo.ec/" />
	<meta property="og:type"          content= "Website" />
	<meta property="og:title"         content="BabyPostal Bebemundo" />
	<meta property="og:description"   content="Crea tu postal personalizada" />
	<meta property="og:image"         content="http://navidad.bebemundo.ec/assets/images/papanoel.png" />
	<title>Bebemundo - Navidad</title>
	<script language="JavaScript" type="text/javascript" src="assets/js/validaciones.js"></script>
	<script language="JavaScript" type="text/javascript" src="assets/js/subirfoto.js"></script>
	<script language="JavaScript" type="text/javascript" src="assets/js/email.js"></script>
	<script language="JavaScript" type="text/javascript" src="assets/js/ajax.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110490022-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-110490022-1');
	</script>

	<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

 </script>
	<link rel="shortcut icon" href="assets/images/gt_favicon.png">
	<!-- Bootstrap itself -->
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!-- Custom styles -->
	<link rel="stylesheet" href="assets/css/magister.css">
	<!-- Fonts -->
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">

			</head>
			<body class="theme-invert" onload="cargarBanner()" style=" overflow-x:hidden;"> <!-- menu y banner -->
				<div class="container-fluid"> <!-- Adds 15px left/right padding -->
				  <div class="row">
						<div class="col-md-12 col-xs-12 center-block ">
							<img  src="assets/images/bannerAzul.png" class="img-responsive "alt="" style="min-width:100%; height:70px;">
						</div>
						<div class="col-md-12 col-xs-12 hidden-lg hidden-md center-block">
							<img  src="assets/images/bannerAzul.png" class="img-responsive center-block"alt="" style="min-width:100%; height:85px;">
						</div>
						<div class="col-xs-12 botones center-block">
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<a href="http://navidad.bebemundo.ec/" ><img src="assets/images/btnBabyPostal.png" class="center-block img-responsive"alt=""></a>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<a href="http://babycatalogo.bebemundo.ec/"><img src="assets/images/btnRopaJuguetes.png" alt="" class="center-block img-responsive"></a>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<a href="http://navidad.bebemundo.ec/site" ><img src="assets/images/btnCuponera.png" alt="" class="center-block img-responsive"></a>
							</div>
						</div>

				<div class="col-xs-12">
				<div class="col-xs-8-push-2 formPostal center-block">	<!-- form postal -->
				<div class="row">
				<div class="col-md-8 col-xs-6">
					<img src="assets/images/header.png" class="img-responsive inline-block pull-right" alt="Cinque Terre">
				</div>
				<div class="col-md-4 col-xs-6">
					<img src="assets/images/Logo-bebemundo.png" class="col-md-6 col-xs-12 img-responsive inline-block pull-right" alt="Cinque Terre" style="min-width:60%;">
				</div>
				</div>
				<!-- Main (Home) section que joda mijo -->
				<div class="container-fluid"> <!-- Adds 15px left/right padding -->
  				<div class="row">
						<div class="col-md-12 col-xs-12">
							<img src="assets/images/bannerNaranja.jpg" class="img-responsive center-block"alt="" style="min-width:100%; ">
							<a id="bannerGif" href="" target="_blank" rel="noopener" >
								<img id="animated" src=""  class="img-responsive center-block"alt="" style="min-width:100%; "> <a/>
						</div>
					</div>
				</div>
				<section >
					<div class="container-fluid">
						<div class="row">
							<form id="formUsers" name="formUsers" action="" >
								<div class="col-md-8 col-lg-8 text-center">
									<div class="form-group-tittle">
										<img src="assets/images/1.png" class="img-responsive inline-block pull-left imgrespo" >
										<h1 class="pull-left" style="font-family:'Gotham Rounded bold';">
											&nbsp;Crea tu postal</h1>
									</div>
										<span>
											<p class="pull-right" style="color:#878787">
												Al crear tu postal no olvides seleccionar un diseño en la parte inferior.
											</p>
										</span>
										<div class="row">
											<div class="position-static col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div classs="fondo">
													<img id="fondo" src= "assets/images/fondo1.png"  width="100%" class="img-responsive shadow">
												</div>
												<div class="foto">
													<img id="foto" src="" class="img-responsive fotoP" >
												</div>
												<div class="marco col-xs-12 col-sm-12 col-md-12 col-lg-12" >
													<img id="marco" src= "assets/images/marco1.png"  width="100%" class="img-responsive" >
												</div>
												<div class="absolutei">
													<textarea type="text" rows="7" name="mensaje" id="mensaje" class="mensaje" placeholder="Escribe tu mensaje aquí" maxlength="120"></textarea>
												</div>
												<div id="absolute" class="image-upload absolute">
													<label for="file-input" >
														<img id="BotonPrevia" src="assets/images/BOTON.png" style="cursor:pointer" width="85%" onclick="preview()"/>
													</label>
													<input name="imagen" id="file-input" type="file" required/>
												</div>
												<img id="fondoO1" src= "assets/images/fondo1.png"  width="100%" class="img-responsive shadow" style="display:none;">
												<img id="marcoO1" src= "assets/images/marco1.png"  width="100%" class="img-responsive shadow" style="display:none;">
												<img id="fondoO2" src= "assets/images/fondo2.png"  width="100%" class="img-responsive shadow" style="display:none;">
												<img id="marcoO2" src= "assets/images/marco2.png"  width="100%" class="img-responsive shadow" style="display:none;">
												<img id="fondoO3" src= "assets/images/fondo3.png"  width="100%" class="img-responsive shadow" style="display:none;">
												<img id="marcoO3" src= "assets/images/marco3.png"  width="100%" class="img-responsive shadow" style="display:none;">
											</div>
										</div>
									<div class="text-left">
										<h3 style="font-family:'Gotham Rounded bold'">
											&nbsp;&nbsp;Seleciona tu diseño
										</h3>
									</div>
									<div class="col-md-3 col-lg-3 col-xs-4">
										<img src="assets/images/postal1.png" style="cursor:pointer" class="img-responsive shadows" onclick = "document.getElementById('selected').value=1; cambiarFondo()">
									</div>
									<div class="col-md-3 col-lg-3 col-xs-4">
										<img src="assets/images/postal2.png" style="cursor:pointer" class="img-responsive shadows" onclick = "document.getElementById('selected').value=2; cambiarFondo()">
									</div>
									<div class="col-md-3 col-lg-3 col-xs-4">
										<img src="assets/images/postal3.png" style="cursor:pointer" class="img-responsive shadows" onclick = "document.getElementById('selected').value=3; cambiarFondo()">
									</div>
									<!-- santa -->
									<div class="col-md-12 col-lg-12 hidden-sm hidden-xs visible-md visible-lg-12 text-center">
										<img src="assets/images/papanoel.png" class="img-responsive inline-block pull-right" alt="Cinque Terre">
									</div> <!-- /col -->
								</div> <!-- /col --><br>
								<div class="col-md-4 col-lg-4 col-xs-12 text-center">
									<div class="form-group-tittle">
										<img src="assets/images/2.png" class="img-responsive inline-block pull-left imgrespo" >
										<h1 class="pull-left">
											&nbsp;Regístrate
										</h1>
									</div>
									<br><br><br>
									<div class="form-group text-center">
										<input type="text" id="name" required class="form-control bbmundo block-center" name="name" placeholder="NOMBRE"><br>
										<input type="text" id="lastName" required class="form-control bbmundo block-center" name="lastName" placeholder="APELLIDO"><br>
										<input type="text" id="ci" required class="form-control bbmundo block-center" name="ci" placeholder="C.I." maxlength="10" onKeyPress="return soloNumeros(event)"><br>
										<input type="email" id="email" required class="form-control bbmundo block-center" name="email" placeholder="EMAIL"><br>
										<input type="hidden" id="selected" required class="form-control bbmundo block-center" name="selected" value="1"  >
										<input type="hidden" id="postalPath" required class="form-control bbmundo block-center" name="postalPath" value=""  >
										<input type="hidden" id="correcto" required class="form-control bbmundo block-center" name="postalPath" value="1"  >
										<a type="submit" id="save"  style="cursor:pointer" onclick="validacionRegistro()" ><img src="assets/images/btnRegistrarme.png"></a>
									</div><br>
									<div class="col-md-12 col-lg-12">
										<div class="hidden-xs">
											<br /><br><br><br><br><br>
										</div>
										<div class="form-group-tittle">
											<img src="assets/images/3.png" class="img-responsive inline-block pull-left imgrespo" >
											<h1 class="pull-left">
												&nbsp;Comparte
											</h1>
										</div>
										<div id="contfb" class="col-md-12 not-active" >
											<a id="fb" href="" target="_blank" rel="noopener"  style="cursor: pointer;">
    									<img class="img-responsive " src="assets/images/f.png" alt="Share on Facebook"></a>
										</div>
										<div id="contEv" class="col-md-12 not-active" >
											<a id="c" href="#popup" class="popup-link " style="cursor: pointer;">
												<img src="assets/images/c.png"  class="img-responsive"></a>
										</div>
										<div id="contDes" class="col-md-12 not-active" >
											<a id="descarga" href="" download="postal.png" style="cursor: pointer;" >
												<img src="assets/images/i.png"  class="img-responsive"></a>

										</div>
										<div>
											<a href="/terminosCondiciones.pdf" download="terminos y condiciones.pdf" style="cursor: pointer; text-align: center;" >  Términos y condiciones  </a>
										</div>
									</div> <!-- /col -->
								</form>
							</div>
							</div>
							</div> <!-- /col -->
						</div> <!-- /row -->
					</div>
				</section>
				<div class="modal-wrapper" id="popup">
					<div class="popup-contenedor ">
						<h2>Envíame por correo electrónico</h2>
						<input type="email" id="destinatario" required class="form-control bbmundo block-center" name="destinatario" placeholder="Ingrese Destinatario Ejm: nombre@email.com" required="required"><br>
						 <span id="emailOK"></span>
						<textarea rows="4"  id="cuerpo" required class="form-control bbmundo block-center" name="mensaje" placeholder="Escriba su mensaje" > </textarea> <br>
						<a type="submit" id="enviar"  class="bts4"style="color:#fff"  onclick="validarEnviar()">Enviar Correo</a>
						<img id="vistaPrevia" class="image-responsive" style="max-width: 100%">
						<a class="popup-cerrar" href="#">X</a>
					</div>
				</div>

				<!-- santa -->
				<div class="col-xs-12 hidden-md hidden-lg visible-xs-12 ">
					<img src="assets/images/papanoel.png" class="img-responsive inline-block pull-right" alt="Cinque Terre">
				</div> <!-- /col -->
				</div>

			</div>
				<!-- Load js libs only when the page is loaded. -->
				<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
				<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
				<script src="assets/js/modernizr.custom.72241.js"></script>
				<!-- Custom template scripts -->
				<script src="assets/js/magister.js"></script>
				<style media="screen">
				@font-face {
					font-family: "Gotham Rounded bold";
					src: url("Gotham Rounded Bold.otf");
				}

				h1, h2, h3 { font-family: 'Gotham Rounded bold', serif; color:#14444E}
				.bbmundo {
					width: 100%;
					padding: 16px 20px;
					border-style: solid;
					border-width: 3px;
					box-shadow: 2px 2px 5px 0;
					border-radius: 18px;
					border-color: #de2a45;
					background-color: transparent;
					color: #3E3E3D;
				}
				::-webkit-input-placeholder { /* WebKit, Blink, Edge */
					color:    #14444E;
				}
				:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
					color:    #14444E;
					opacity:  1;
				}
				::-moz-placeholder { /* Mozilla Firefox 19+ */
					color:    #14444E;
					opacity:  1;
				}
				:-ms-input-placeholder { /* Internet Explorer 10-11 */
					color:    #14444E;
				}
				::-ms-input-placeholder { /* Microsoft Edge */
					color:    #14444E;
				}
				.bts {
					color: white;
					padding: 20px;
					text-align: center;
					text-decoration: none;
					display: inline-block;
					font-size: 26px;
					margin: 4px 2px;
					cursor: pointer;
				}
				.bts4 {
					background-color: #a01c26;
					color: #fff;
					padding: 10px;
					text-align: center;
					text-decoration: none;
					display: inline-block;
					font-size: 26px;
					margin: 4px 2px;
					cursor: pointer;
					border-radius: 35px;
					box-shadow: 8px 8px 10px 0;
				}
				.image-upload > input
							{
								display: none;

							}

							div.relative {
								position: relative;
								width: 100%;
								height: auto;
							}
							div.absolute {
								position: absolute;
								top: 40%;
								cursor: pointer;
								right: 50px;
								width: 30%;
								z-index: 4;
							}
							div.absolutei {
								position: absolute;
								top: 35%;
								border-width: medium;
								border-color: red;
								color: #14444E;
								font-size: 2.5em;
								left: 20px;
								width: 30%;
								height: 100px;
								z-index: 4;
							}

							div.fondo {
								position: absolute;
								top: 0px;
								z-index: 1;

							}
							div.foto {
								position: absolute;
								top: 45px;
								left: 515px;
								width: 250px;
								min-height: 400px;
								max-height: 400px;
								z-index: 2;
							}
							.fotoP
							{
								position: absolute;
								width: 400px;
								min-height: 400px;
								z-index: 2;
							}
							div.marco {
								position: absolute;
								top: 0px;
								z-index: 3;
							}
							.shadow{
								border-style: solid;
								border-color: white;
								border-width: 10px;
								box-shadow: 10px 10px 5px #888888;
							}
							.shadows{
								border-style: solid;
								border-color: white;
								border-width: 5px;
							}





							.mensaje{
								border: none;
								background-color: transparent;
								font-size: 0.7em;
								padding-left: 40px;
								resize: none;
								width:380px;
								padding: 10px;
								color: #174657;
								}
								.mensaje:focus{
									outline: 0 !important;
									border:0 none !important;
									color: #174657;
							}

							.facebookButton
							{
								width: 123px;
								height: 123px;
								z-index: 6;
							}
							.share
							{
								width: 123px;
								height: 123px;
								display:none;
								z-index: 7;
							}
							#popup {
								display: none;
								visibility: hidden;
								opacity: 0;
								margin-top: -300px;
							}
							#popup:target {
								display: block;
								visibility:visible;
								opacity: 1;
								background-color: rgba(231,037,018,0.8);
								position: fixed;
								top:0;
								left:0;
								right:0;
								bottom:0;
								margin:0;
								z-index: 999;
								transition:all 1s;
							}
							.popup-contenedor {
								position: relative;
								margin:2% auto;
								padding:30px 50px;
								background-color: #fafafa;
								color:#333;
								border-radius: 3px;
								width:50%;
							}
							a.popup-cerrar {
								position: absolute;
								top:3px;
								right:3px;
								background-color: #333;
								padding:7px 10px;
								font-size: 20px;
								text-decoration: none;
								line-height: 1;
								color:#fff;
							}
							div.bannerAzul
							{
									position: absolute;
									min-width: 100%;
									z-index: 8;
							}
							div.botones

							{
									width: 100%;
									top: 10px;
									position: absolute;
									z-index: 9;
							}
							div.formPostal
							{

								max-width: 1200px;
							}
							.not-active {
								 pointer-events: none;

								 cursor: default;
							}
							.active {

								 opacity: 1;

							}

							.form-group-tittle {
								display: flex;
								align-items: center;
						}
						@media only screen and (max-width: 1200px) and (min-width: 821px) {
							div.absolute {
								position: absolute;
								top: 35%;
								cursor: pointer;
								right: 35px;
								width: 30%;
								height: 10px;
								z-index: 4;
							}
							.popup-contenedor {
								position: relative;
								/*margin:2% auto;*/
								/*padding:30px 50px;*/
								background-color: #fafafa;
								color:#333;
								border-radius: 3px;
								width:100%;
							}
										div.bannerAzul
										{
												position: absolute;
												min-width: 100%;
												z-index: 8;
												height: 50px;
										}
										div.botones

										{
												width: 100%;
												top: 10px;
												position: absolute;
												z-index: 9;
										}
										div.formPostal
										{

											max-width: 700px;
										}
										h1{
											font-size: 1em;
										}
										p{
											font-size: 0.7em;
										}
										h3{
											font-size: 0.8em;
										}
										.mensaje{
											border: none;
											background-color: transparent;
											font-size: 0.6em;
											padding-left: 50px;
											resize: none;
											width:380px;
											padding: 0px;
											padding-top: 0px;
											line-height:20px;
											color: #174657;
											}
											.mensaje:focus{
												outline: 0 !important;
												border:0 none !important;
												color: #174657;
										}
										.form-group-tittle {
											display: block;
											align-items: center;
									}
									div.foto {
										position: absolute;
										top: 25px;
										left: 315px;
										min-height: 220px;
										max-height: 220px;
										z-index: 2;

									}
									.fotoP{
										position: absolute;
										width: 140px;
										min-height: 220px;
										z-index: 2;
									}
									.imgrespo{
										width: 50px;
								}
								.shadow{
									border-style: solid;
									border-color: white;
									border-width: 5px;
									box-shadow: 6px 6px 5px #888888;
								}
								.shadows{
									border-style: solid;
									border-color: white;
									border-width: 3px;
								}

								}
						@media only screen and (max-width: 820px) and (min-width: 621px) {
							.popup-contenedor {
								position: relative;
								/*margin:2% auto;*/
								/*padding:30px 50px;*/
								background-color: #fafafa;
								color:#333;
								border-radius: 3px;
								width:100%;
							}
										div.bannerAzul
										{
												position: absolute;
												min-width: 100%;
												z-index: 8;
												min-height: 150px;
												max-height: 150px;
										}
										div.botones

										{
												width: 100%;
												top: 10px;
												position: absolute;
												z-index: 9;
										}
										div.formPostal
										{

											max-width: 700px;
										}
										h1{
											font-size: 1em;
										}
										p{
											font-size: 0.7em;
										}
										h3{
											font-size: 0.8em;
										}
										.mensaje{
											border: none;
											background-color: transparent;
											font-size: 0.7em;
											padding-left: 50px;
											resize: none;
											width:380px;
											padding: 0px;
											padding-top: 0px;
											line-height:20px;
											color: #174657;
											}
											.mensaje:focus{
												outline: 0 !important;
												border:0 none !important;
												color: #174657;
										}
										.form-group-tittle {
											display: block;
											align-items: center;
									}
									div.foto {
										position: absolute;
										top: 40px;
										left: 460px;
										min-height: 360px;
										max-height: 360px;
										z-index: 2;

									}
									.fotoP{
										position: absolute;
										width: 225px;
										min-height: 360px;
										z-index: 2;
									}
									.imgrespo{
										width: 50px;
								}
								.shadow{
									border-style: solid;
									border-color: white;
									border-width: 5px;
									box-shadow: 6px 6px 5px #888888;
								}
								.shadows{
									border-style: solid;
									border-color: white;
									border-width: 3px;
								}

								}
								@media only screen and (max-width: 620px) and (min-width: 501px) {
									div.bannerAzul
									{
											position: absolute;
											min-width: 100%;
											z-index: 8;
											height: 50px;
									}
									.popup-contenedor {
										position: relative;
										/*margin:2% auto;*/
										/*padding:30px 50px;*/
										background-color: #fafafa;
										color:#333;
										border-radius: 3px;
										width:100%;
									}
									div.botones

									{
											width: 100%;
											top: 10px;
											position: absolute;
											z-index: 9;
									}
									div.formPostal
									{

										max-width: 700px;
									}
									h1{
										font-size: 1em;
									}
									p{
										font-size: 0.4em;
									}
									h3{
										font-size: 0.8em;
									}
									div.absolute {
										position: absolute;
										top: 30%;
										cursor: pointer;
										right: 20px;
										width: 30%;
										height: 10px;
										z-index: 4;
									}
									div.absolutei {
										position: absolute;
										top: 0%;
										border-width: medium;
										border-color: red;
										color: #14444E;
										font-size: 5em;
										line-height: 5cm;
										left: 25px;
										width: 30%;
										height: 100px;
										z-index: 4;
									}
									.marco {
										position: absolute;
										top: 0px;
										left:4px;
										z-index: 3;
									}
									.mensaje{
										border: none;
										background-color: transparent;
										font-size: 0.2em;
										padding-left: 30px;
										resize: none;
										width:110px;
										padding: 0px;
										padding-top: 0px;
										line-height:10px;
										color: #174657;
										}
										.mensaje:focus{
											outline: 0 !important;
											border:0 none !important;
											color: #174657;
									}
									div.foto {
										position: absolute;
										top: 10px;
										left: 180px;
										min-height: 180px;
										max-height: 180px;
										z-index: 2;

									}
									.fotoP{
										position: absolute;
										width: 86px;
										min-height: 145px;
										z-index: 2;
									}
									.form-group-tittle {
										display: block;
										align-items: center;
								}

								.imgrespo{
									width: 50px;
							}
							.shadow{
								border-style: solid;
								border-color: white;
								border-width: 5px;
								box-shadow: 6px 6px 5px #888888;
							}
							.shadows{
								border-style: solid;
								border-color: white;
								border-width: 3px;
							}
						}

					@media only screen and (max-width: 500px) and (min-width: 341px) {

						div.botones

						{
								width: 110%;
								top: 10px;
								position: absolute;
								z-index: 9;
						}
						.popup-contenedor {
							position: relative;
							/*margin:2% auto;*/
							/*padding:30px 50px;*/
							background-color: #fafafa;
							color:#333;
							border-radius: 3px;
							width:100%;
						}
						div.formPostal
						{

							max-width: 320px;
						}
						h1{
							font-size: 1em;
						}
						p{
							font-size: 0.5em;
						}
						h3{
							font-size: 0.8em;
						}
						div.absolute {
							position: absolute;
							top: 30%;
							cursor: pointer;
							right: 20px;
							width: 30%;
							height: 10px;
							z-index: 4;
						}
						div.absolutei {
							position: absolute;
							top: 0%;
							border-width: medium;
							border-color: red;
							color: #14444E;
							font-size: 5em;
							line-height: 5cm;
							left: 25px;
							width: 30%;
							height: 100px;
							z-index: 4;
						}
						.marco {
							position: absolute;
							top: 0px;
							left:4px;
							z-index: 3;
						}
						.mensaje{
							border: none;
							background-color: transparent;
							font-size: 0.2em;
							padding-left: 30px;
							resize: none;
							width:140px;
							padding: 10px;
							padding-top: 50px;
							line-height:10px;
							color: #174657;
							}
							.mensaje:focus{
								outline: 0 !important;
								border:0 none !important;
								color: #174657;
						}
						div.foto {
							position: absolute;
							top: 15px;
							left: 215px;
							width: 100px;
							min-height: 168px;
							max-height: 168px;
							z-index: 2;

						}
						.fotoP{
							position: absolute;
							width: 100px;
							min-height: 168px;
							z-index: 2;
						}
						.form-group-tittle {
							display: block;
							align-items: center;
					}

					.imgrespo{
						width: 50px;
					}
					.shadow{
					border-style: solid;
					border-color: white;
					border-width: 5px;
					box-shadow: 6px 6px 5px #888888;
					}
					.shadows{
					border-style: solid;
					border-color: white;
					border-width: 3px;
					}
				}
				@media only screen and (max-width: 340px) and (min-width: 5px)  {

						div.botones

						{
								width: 110%;
								top: 10px;
								position: absolute;
								z-index: 9;
						}
					.popup-contenedor {
						position: relative;
						/*margin:2% auto;*/
						/*padding:30px 50px;*/
						background-color: #fafafa;
						color:#333;
						border-radius: 3px;
						width:100%;
					}
					h1{
						font-size: 1em;
					}
					p{
						font-size: 0.4em;
					}
					h3{
						font-size: 0.8em;
					}
					div.absolute {
						position: absolute;
						top: 30%;
						cursor: pointer;
						right: 20px;
						width: 30%;
						height: 10px;
						z-index: 4;
					}
					div.absolutei {
						position: absolute;
						top: 0%;
						border-width: medium;
						border-color: red;
						color: #14444E;
						font-size: 5em;
						line-height: 5cm;
						left: 25px;
						width: 30%;
						height: 100px;
						z-index: 4;
					}
					.marco {
						position: absolute;
						top: 0px;
						left:4px;
						z-index: 3;
					}
					.mensaje{
						border: none;
						background-color: transparent;
						font-size: 0.2em;
						padding-left: 30px;
						resize: none;
						width:130px;
						padding: 10px;
						padding-top: 50px;
						line-height:10px;
						color: #174657;
						}
						.mensaje:focus{
							outline: 0 !important;
							border:0 none !important;
							color: #174657;
					}
					div.foto {
						position: absolute;
						top: 10px;
						left: 215px;
						min-height: 167px;
						max-height: 167px;
						z-index: 2;

					}
					.fotoP{
						position: absolute;
						width: 105px;
						min-height: 167px;
						z-index: 2;
					}
					.form-group-tittle {
						display: block;
						align-items: center;
				}

				.imgrespo{
					width: 50px;
				}
				.shadow{
				border-style: solid;
				border-color: white;
				border-width: 5px;
				box-shadow: 6px 6px 5px #888888;
				}
				.shadows{
				border-style: solid;
				border-color: white;
				border-width: 3px;
				}
			}


					</style>
			</body>
			</html>
