
// Función para recoger los datos de PHP según el navegador, se usa siempre.
function objetoAjax()
{
	var xmlhttp=false;
	try
	{
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch (e)
	{

		try
		{
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch (E)
		{
			xmlhttp = false;
		}
}

if (!xmlhttp && typeof XMLHttpRequest!='undefined')
{
	  xmlhttp = new XMLHttpRequest();
}
return xmlhttp;
}


function preview ()
{

document.getElementById("file-input").onchange = function() {
  var reader = new FileReader(); //instanciamos el objeto de la api FileReader

  reader.onload = function(e) {
		var fileSize = $('#file-input')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);
    if (siezekiloByte >  5120) {
        toastError("Su foto debe tener un tamaño máximo de 5mb, las fotos que recibes por whatsapp son una buena opción");
				document.getElementById("foto").src = "";
        return false;
    }
		else {
			document.getElementById("foto").src = e.target.result;
			document.getElementById("BotonPrevia").src ="assets/images/cambiar-foto.png";
			document.getElementById("absolute").style.top = "90%";
			document.getElementById("correcto").value = "3";
		}


    //actualizar( );


  };

  // read the image file as a data URL.
  reader.readAsDataURL(this.files[0]);
};

}
function cambiarFondo ()
{
	var seleccion = document.getElementById("selected").value;
	//document.getElementById("fondo").src= 'assets/images/fondo' + seleccion + '.png'
	//document.getElementById("marco").src= 'assets/images/marco' + seleccion + '.png'

	document.getElementById("fondo").src= document.getElementById('fondoO'+seleccion).src;
	document.getElementById("marco").src= document.getElementById('marcoO'+seleccion).src;
}

function actualizar(){
	name=document.getElementById('name').value;
	lastName=document.getElementById('lastName').value;
	ci=document.getElementById('ci').value;
	email=document.getElementById('email').value;
	escojido=document.getElementById('selected').value;
	texto=document.getElementById('mensaje').value;
	dirFoto=document.getElementById('foto').src;
	//path = "http://navidad.bebemundo.ec/demo/posta/FELIZ-NAVIDAD-1511150916.png" ;
        var parametros = {
                "escojido" : escojido,
                "texto" : texto,
								"directorioFoto" : dirFoto,
								"name": name,
								"lastName": lastName,
								"ci": ci,
								"email": email
        };
        $.ajax({
                data:  parametros,
                url:   'test.php',
                type:  'post',

                beforeSend: function () {

												toastProceso();
                },
                success:  function (response) {

												document.getElementById('postalPath').value=response;
												document.getElementById('fb').href='https://www.facebook.com/sharer/sharer.php?u=http://navidad.bebemundo.ec/'+response;
												document.getElementById('descarga').href=response;
												document.getElementById('vistaPrevia').src=response;

												document.getElementById('fb').style.pointerEvents = "all";
												document.getElementById('c').style.pointerEvents = "all";
												document.getElementById('descarga').style.pointerEvents = "all";

												document.getElementById('fb').style.cursor = "pointer";
												document.getElementById('c').style.cursor = "pointer";
												document.getElementById('descarga').style.cursor = "pointer";
												enviarRegistro();
												toastPostal();


                }
        });

}

function cargarBanner()
{
	var x = Math.floor((Math.random() * 2) + 1);
  if(x==1)
	{
		document.getElementById('animated').src="assets/images/bannerA3.gif";
		document.getElementById('bannerGif').href="http://navidad.bebemundo.ec/";
	}
	else if(x==2)
	{
		document.getElementById('animated').src="assets/images/bannerA2.gif";
		document.getElementById('bannerGif').href="http://navidad.bebemundo.ec/site";
	}
	else {
		document.getElementById('animated').src="assets/images/bannerA2.gif";
		document.getElementById('bannerGif').href="http://navidad.bebemundo.ec/";
	}
}

function toastPostal()
{
	//toastr.options.timeOut = 1500;
	toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
	//toastr.success('Gracias por crear tu Postal');
	Command: toastr["info"]("Su postal se a creado con exito, ahora puede compartirla.", "Gracias");

}
function toastCorreo()
{
	toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}


Command: toastr["info"]("Su postal ha sido enviada con exito por correo electronico", "Gracias");

}

function toastError(texto)
{
	var mensaje = texto;
	toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}


Command: toastr["error"](mensaje, "Error");

}
function toastProceso()
{

	toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-top-full-width",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}


Command: toastr["warning"]("Su postal esta siendo creada, este proceso puede durar varios segundos, favor espere la alerta de confirmacion antes de compartir su postal", "información");

}
