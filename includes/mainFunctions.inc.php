<?php

// Constantes conexión con la base de datos
include('conexion.php');

// Variable que indica el status de la conexión a la base de datos
$errorDbConexion = false;


// Función para extraer el listado de usurios
function consultaUsers($linkDB){
	$salida = '';
	$consulta = $linkDB -> query("SELECT id,name,lastName,ci,email
								  FROM users ORDER BY name ASC Limit 1");
	if($consulta -> num_rows != 0){

		// convertimos el objeto
		while($listadoOK = $consulta -> fetch_assoc())
		{
			$salida .= '
				<tr>
					<td>'.$listadoOK['name'].'</td>
					<td>'.$listadoOK['lastName'].'</td>
					<td>'.$listadoOK['email'].'</td>	<tr>
			';
		}

	}
	else{
		$salida = '
			<tr id="sinDatos">
				<td colspan="5" class="centerTXT">NO HAY REGISTROS EN LA BASE DE DATOS</td>
	   		</tr>
		';
	}

	return $salida;
}

// Verificar constantes para conexión al servidor
if(defined('server') && defined('user') && defined('pass') && defined('mainDataBase'))
{
	// Conexión con la base de datos

	$mysqli = new mysqli(server, user, pass, mainDataBase);

	// Verificamos si hay error al conectar
	if (mysqli_connect_error()) {
	    $errorDbConexion = true;
	}

	// Evitando problemas con acentos
	$mysqli -> query('SET NAMES "utf8"');
}


?>
